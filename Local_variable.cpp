/*A variable decleared within a block ,method/fuction is called local variable .This variable is created when entered into a block/method or destroyed when exit from block/function*/


#include<iostream>

using namespace std;

int main(){

 int a_condition = 10 ;

 if(a_condition < 20)
{
    int local_variable_inside_if_condition = 30;
    printf("Inside the condition : %d",local_variable_inside_if_condition);
} 
// we can't use local_variable_inside_if_condition here as variable will be destroyed at the end of the if condition
//printf("Outside the condition : %d",local_variable_outside_if_condition);

    return 0;
}