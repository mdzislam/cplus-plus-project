//C++ example to illustrate the auto storage class
#include <iostream>

using namespace std;

void autoStorageClass(){

    cout << "Example of auto storage class \n";

    //Declaring an auto variable 
    int   a =  32;
    float b = 3.2 ;
    string c = "GeeksforGeeks";
    char d = 'G';

    //Printing the auto variables

    cout << a << " \n ";
    cout << b << " \n ";
    cout << c << " \n ";
    cout << d << " \n ";
}

int main(){
 // To demonstrate auto storage class
 autoStorageClass();
 return 0;

}