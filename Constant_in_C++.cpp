/*One thing to note here is that we have to initialize the constant variables at declaration*/

#include <iostream>

using namespace std;

int main(){
//Defining interfer constant using const keyword
const int int_const = 25 ;

//Defining character constant using const keyword
const char char_const = 'A';

//Defining float constant using const keyword
const float float_const = 15.66 ;

printf("Printing value of Integer Constant: %d \n",int_const);
printf("Printing value of Character Constant: %c \n",char_const);
printf("Printing value of Float Constant : %f \n",float_const);

return 0;
}