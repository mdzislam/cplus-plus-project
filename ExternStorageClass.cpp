// Example of extern Storage Class
#include <iostream>
#include "ExternStorageClass_external.cpp"
using namespace std;

// Declaring a  extern variable 
int x;
extern int y;
void externStorageClass(){
 //Printing the extern variable x
 x = 20;

 cout << "Printing value of x :"<< x<< endl;
 cout << "Printing value of y from other file :"<< y <<endl;
 y = 25;
cout << "Printing value of y from other file :"<< y <<endl;

}

int main(){

    // To demonstrate extern Storage class
    externStorageClass();
    return 0;
}